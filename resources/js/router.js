import Vue from 'vue'
import VueRouter from 'vue-router'
import ExampleComponent from './components/ExampleComponent.vue'
Vue.use(VueRouter)



const routes = [
   
    {
       
        path: "/welcome/example-component",

        name: "example-component",
        component: ExampleComponent,
       
    }]


